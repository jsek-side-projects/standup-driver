# Standup Board

Live at [standup-board.netlify.com](https://standup-board.netlify.com)

![](./screenshots/TimerMode.png)

![](./screenshots/InfoPanel.png)

## Project setup
```
npm install
```

### Development
```
npm run serve
```

### Production build
```
npm run build
```

<div>
Favicon made by <a href="https://www.freepik.com/">Freepik</a>
from <a href="https://www.flaticon.com/">www.flaticon.com</a>
is licensed by <a href="http://creativecommons.org/licenses/by/3.0/">CC 3.0 BY</a>
</div>
import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'

Vue.use(Vuex)

const persist = new VuexPersist({
  key: 'standup-driver',
  storage: localStorage
})

export default new Vuex.Store({
  plugins: [persist.plugin],
  modules: {
    app: {
      namespaced: true,
      state: {
        displayMode: 'timer'
      },
      mutations: {
        openTimer(state) {
          state.displayMode = 'timer'
        },
        openSettings(state) {
          state.displayMode = 'settings'
        },
        openInfoPanel(state) {
          state.displayMode = 'info'
        }
      }
    },
    timer: {
      namespaced: true,
      state: {
        isRunning: false,
        startTime: 0,
      },
      mutations: {
        reset(state) {
          state.startTime = 0
          state.isRunning = false
        },
        toggleRunning(state) {
          if (state.isRunning) { 
            state.isRunning = false
          } else {
            state.startTime = state.startTime || Date.now()
            state.isRunning = true
          }
        }
      }
    },
    members: {
      namespaced: true,
      state: {
        members: [{
          // isActive: false,
          // isDone: false,
          // hasWarning: false
        }],
      },
      mutations: {
        add(state) {
          state.members.push({})
        },
        remove(state) {
          state.members.pop()
        },
        setWarning(state) {
          const activeMember = state.members.find((m: any) => m.isActive)
          if (activeMember) {
            activeMember.hasWarning = true
          }
          state.members = [...state.members]
        },
        setDone(state) {
          const activeMember = state.members.find((m: any) => m.isActive)
          if (activeMember) {
            activeMember.isDone = true
            delete activeMember.hasWarning
          }
          state.members = [...state.members]
        },
        selectNext(state) {
          const activeMember = state.members.find((m: any) => m.isActive)
          const activeMemberIndex = state.members.indexOf(activeMember)
          if (activeMember) {
            delete state.members[activeMemberIndex].isActive
          }
          if (activeMemberIndex + 1 < state.members.length) {
            state.members[activeMemberIndex + 1].isActive = true
          }
          state.members = [...state.members]
        },
        reset(state) {
          state.members = state.members.map(() => ({}))
        }
      }
    },
    checklist: {
      namespaced: true,
      state: {
        points: [
          { title: 'Progress', description: 'tasks done & your current task' },
          { title: 'Plan', description: 'your plan for today' },
          { title: 'Problems', description: 'anything preventing you from executing the plan' },
        ]
      },
      mutations: {
        toggle(state, pointIndex) {
          if (pointIndex >= state.points.length) {
            return
          }
          const point = state.points[pointIndex]
          if (point.isDone) {
            delete point.isDone
          } else {
            point.isDone = true
          }
          state.points = [...state.points]
        },
        reset(state) {
          state.points.forEach((p: any) => delete p.isDone)
          state.points = [...state.points]
        }
      }
    }
  }
})

import Vue from 'vue'
import Vuetify from 'vuetify'
// import 'vuetify/dist/vuetify.min.css'
import '../style/main.styl'

Vue.use(Vuetify, {
  theme: {
    primary: '#1D6FA3',
    // secondary: '',
    // accent: '',
    // error: '',
    // info: '',
    // success: '',
    // warning: ''
  },
  iconfont: 'md'
})
